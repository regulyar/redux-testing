import {
	ADD_USER
} from "../constants/registration";

const defaultState = {
    id: 0,
};

export const addUserReducer = (state=defaultState, action) => {
	switch (action.type) {
		case ADD_USER:
        	return {...defaultState, id: state.id + 1};
		default:
			return state;
	}
};
