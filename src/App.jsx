import React from 'react';
import logo from './logo.svg';
import './App.css';
import {connect} from "react-redux";
import {addUser} from "./actions";

export class _App extends React.Component {
  state = {
    id: 0,
  };

  componentDidMount() {
    this.setState({
      id: 0
    })
  }


  render() {
      console.log(this.props.id);
    return (
    <div className="App">

      <header className="App-header">
        <p>
          Hello, there.
        </p>
      </header>
      
      <input className="App-input" type="input" placeholder="Master Kenobi">
      </input>

      <input className="App-input" type="input" placeholder="Master Kenobi">
      </input>

      <button onClick={() => this.props.addUser(42)}>
        click
      </button>
      <button>
      {this.props.id}
      </button>

    </div>
  )}
}

const mapStateToProps = (state) => ({
    id: state.id,
});

const mapDispatchToProps = (dispatch) => ({
    addUser: id => dispatch(addUser(id))
});

export const App = connect(
  mapStateToProps,
  mapDispatchToProps
)(_App);
export default App;
