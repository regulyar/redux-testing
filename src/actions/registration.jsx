import {ADD_USER} from '../constants/registration'

export const addUser = id => ({
    type: ADD_USER,
    payload: id
});
